# Script for statistics on coding sheets

#### Load packages and files #### ----
source("script_to_import_coding_sheets.R") # tidyverse becomes loaded
coding.all<-filter(coding.all, include_exclude=="include", na.rm=TRUE) 

##### overall statistics ----


# Number of papers matching all_data count?
length(unique(coding.all$study_id)) == 171 # ok

# Number of evidence points
length((coding.all$study_id))

# Number of publications per hypothesis
coding.all %>% # counting papers, not evidence points (remove this line if otherwise)
  distinct(study_id, .keep_all = TRUE) %>%
  select(general_hypothesis) %>%
  table()

# Number of authors per publication
coding.all %>%
  distinct(title, .keep_all = TRUE) %>%
  mutate(author = strsplit(as.character(author), ", ")) %>% 
  unnest(author) %>%
  select(author) %>%
  arrange() %>%
  table() %>%
  sort()
  #length()

# Have rodents and their food plants been related to changed population dynamics and/or climate change?
coding.all %>%
  filter(context != "not_reported") %>%
  distinct(study_id, .keep_all = TRUE) %>%
  select(study_id, context, title)

coding.all %>%
  filter(dynamics_effect != "not_reported" & dynamics_effect != "not_relevant") %>%
  distinct(study_id, .keep_all = TRUE) %>%
  select(study_id, context, dynamics_effect)



# Plot number of publications per hypothesis over time
col.hyp <- c("#000000", "#CC79A7", "#009E73", "#d8b365") # Set permanent color coding on hypotheses. Feel free to change...
labels = c("A Inherent plant cycles", 
           "B Food quantity", 
           "C Food quality of preferred food",
           "D Food quality due to dietary change")

df <- coding.all %>%
  distinct(study_id, .keep_all = TRUE) %>% # counting papers, not evidence points (remove this line if otherwise)
  arrange(general_hypothesis, publication_year)

plot.cumulative = data.frame(hypothesis = df$general_hypothesis, 
                           count = ave(df$general_hypothesis == df$general_hypothesis, #cumulative count per hypothesis
                                       df$general_hypothesis,
                                       FUN = cumsum), 
                           year = df$publication_year)

plot.cumulative %>%
  ggplot(aes(year, count,
             color = hypothesis)) +
  geom_line(size = 1, alpha = 0.8) +
  scale_color_manual(name = "Hypothesis",
                     values = col.hyp,
                     labels = labels) +
  ylab("Accumulated number of publications") +
  xlab("Year of publication") +
  theme_light() +
  theme(legend.position = c(.33, .8),
        legend.direction = "vertical",
        legend.background = element_rect(linetype="solid",
                                         color = "black"))

ggsave("./../../figures/Figure_3_accumulated_number_of_publications.jpg",
#ggsave("./../../figures/Figure_3_accumulated_number_of_publications.pdf",
  width = 5, height = 5)

# Bias 
read_excel("./../coding sheets/coding_sheet_excluded.xls", 
           sheet = 1)[-1:-3] %>% View() #high risk
coding.all %>%
  filter(suspectibility_to_bias == "medium") %>%
  select(suspectibility_to_bias_comment) %>%
  View()

# Selection: How many papers do we not comment on lacking
coding.all %>%
  filter(suspectibility_to_bias == "low") %>%
  filter(!str_detect(selection, "not described"),
         !str_detect(selection, "neither is described"),
         !str_detect(selection, "not further described"),
         !str_detect(selection, "no further rationale"),
         !str_detect(selection, "selecting these areas not given"),
         !str_detect(selection, "neither is described")) %>%
  #select(selection) %>%
  #clipr::write_clip() 
  nrow()

# On average, how many evidence points per paper?
length(coding.all$study_id) / length(unique(coding.all$study_id)) # ca 1.5 ep. per paper.

# How many of papers state that they test a hypothesis related to cycl. dynamics?
length(which(coding.all$question_type == "yes")) / length(coding.all$question_type) # 82 %

# Span of data collection vs study design
coding.all %>%
  mutate(year_sum = year_end - year_start) %>% # 0 means less than 1, 1 means between 1 and 2 years duration etc.
  drop_na(year_end) %>% # n=14 does not have data on duration
  filter(study_design != "other") %>% # Replace Hamback study with other that contained both quasi- and experimental design
  add_row(study_design = "experimental", year_sum = 3) %>%
  add_row(study_design = "experimental", year_sum = 3) %>%
  
  ggplot(aes(study_design, year_sum)) +
  geom_boxplot() +
  xlab("Study design") +
  scale_y_log10(breaks = c(1, 2, 4, 10, 50), labels = c("0-1 year", "1-2 years", "4 years", "10 years", "50 years")) +
  scale_x_discrete(limits = rev(c("observational", 
                                    "experimental", 
                                    "quasi-experimental")),
                     labels = rev(c("Observational",
                                    "Experimental",
                                    "Quasi-experimental"))) +
  ylab("Length of study") +
  coord_flip() +
  theme_light()

ggsave("./../../figures/Figure_S1_span_of_study_vs_design.jpg",
       #ggsave("./../../figures/Figure_6_span_of_study_vs_design.pdf",
       width = 6, height = 3)


#### Time of publication span:
paste(min(coding.all$publication_year), "-", max(coding.all$publication_year))

#### Number of rodent species
toto<-coding.all %>%
  mutate(rodent = strsplit(as.character(rodent), ", ")) %>% 
  unnest(rodent) %>%
  select(rodent) %>%
  arrange() %>%
  table() %>%
  prop.table()
toto

#lemmings
toto<-as.data.frame(toto)
sum(toto[c(4,5,7,8),2])

coding.all %>%
  mutate(rodent = strsplit(as.character(rodent), ", ")) %>% 
  unnest(rodent) %>%
  select(rodent) %>%
  mutate(rodent = strsplit(as.character(rodent), "_")) %>% 
  unnest(rodent) %>%
  table()


#### Number of plant species
coding.all %>%
  mutate(plant = strsplit(as.character(plant), ", ")) %>% 
  unnest(plant) %>%
  select(plant) %>%
  mutate(plant = strsplit(as.character(plant), "_")) %>% 
  unnest(plant) %>%
  arrange() %>%
  table() %>%
  sort() 

#### Study design
table(coding.all$study_design)
coding.all %>% 
  filter(assumption == "B_i) AND D_i) Rodent_p feeding induces quantitative changes in their preferred food plants") %>%
  filter(study_design == "experimental") %>%
  select(approach_cycle) %>% table()


#### Dampening
coding.all %>% # not reported (n=232)
  filter(context != "not_reported") %>%
  select(general_hypothesis, assumption, dynamics_effect) %>%
  View()


toto<-coding.all %>% select(c(id, dynamics_effect)) %>% filter(dynamics_effect != "not_reported") %>% filter(dynamics_effect != "not_relevant")
toto


###### stats HA ----

names(coding.all)
coding.all$general_hypothesis<-as.factor(coding.all$general_hypothesis)
HA<-filter(coding.all, general_hypothesis=="A_inherent_plant_cycles")
HA<-filter(HA, include_exclude=="include", na.rm=TRUE) 

dim(HA)
table(HA$plant)


# temperate trees
tree_masting<-filter(HA, grepl("tree_masting", comment))
dim(tree_masting)

fag<-filter(tree_masting, grepl("Fagus", tree_masting$plant))
quer<-filter(tree_masting, grepl("Quercus", tree_masting$plant))
fag_que<-rbind.data.frame(fag, quer)
length(unique(fag_que$id))

table(tree_masting$biome)
table(tree_masting$plant)

pic<-filter(tree_masting, grepl("Picea", tree_masting$plant))
pin<-filter(tree_masting, grepl("Pinus", tree_masting$plant))
abi<-filter(tree_masting, grepl("Abies", tree_masting$plant))
tsu<-filter(tree_masting, grepl("Tsuga", tree_masting$plant))
ptsu<-filter(tree_masting, grepl("Pseudotsuga", tree_masting$plant))
conifers <- do.call("rbind", list(pic, pin, abi, tsu, ptsu))
length(unique(conifers$id))

table(tree_masting$rodent)
table(tree_masting$assumption)

A_add<-filter(tree_masting, grepl("A_additional", tree_masting$assumption))
A_add<-filter(A_add, grepl("low", A_add$suspectibility_to_bias))
dim(A_add)
A_add$assumption_support

A_i<-filter(tree_masting, grepl("A_i)", tree_masting$assumption))
A_i$assumption_support
A_i$suspectibility_to_bias


A_ii<-filter(tree_masting, grepl("A_ii)", tree_masting$assumption))
A_ii$assumption_support
A_ii$suspectibility_to_bias

A_iii<-filter(tree_masting, grepl("A_iii)", tree_masting$assumption))
A_iii$assumption_support
A_iii$suspectibility_to_bias

A_iv<-filter(tree_masting, grepl("A_iv)", tree_masting$assumption))
A_iv$assumption_support
A_iv$suspectibility_to_bias

# boreal berries
ericoid_berry<-filter(HA, grepl("ericoid_berries", comment))
dim(ericoid_berry)

table(ericoid_berry$rodent)
table(ericoid_berry$assumption)


A_i<-filter(ericoid_berry, grepl("A_i)", ericoid_berry$assumption))
A_i$assumption_support
A_i$suspectibility_to_bias

A_add<-filter(ericoid_berry, grepl("A_additional", ericoid_berry$assumption))
A_add$assumption_support
A_add$suspectibility_to_bias
low<-filter(A_add, grepl("low", suspectibility_to_bias))
low$assumption_support
dim(A_add)
medium<-filter(A_add, grepl("medium", suspectibility_to_bias))
medium$assumption_support

table(ericoid_berry$author)
sum(grepl("Selås", ericoid_berry$author))

table(ericoid_berry$author, ericoid_berry$country)
table(ericoid_berry$biome)
table(ericoid_berry$author, ericoid_berry$country, ericoid_berry$biome)

# others
other<-filter(HA, grepl("Group: other", comment))
dim(other)

table(other$rodent)
table(other$assumption)
table(other$biome)
table(other$habitat)


A_i<-filter(other, grepl("A_i)", assumption))
low<-filter(A_i, grepl("low", suspectibility_to_bias))
low$assumption_support
table(low$habitat, low$biome)

A_add<-filter(other, grepl("A_add", assumption))
A_add$assumption_support
low<-filter(A_add, grepl("low", suspectibility_to_bias))
low$assumption_support
table(low$habitat, low$biome)

A_ii<-filter(other, grepl("A_ii)", assumption))
A_ii$assumption_support

A_iii<-filter(other, grepl("A_iii)", assumption))
A_iii$assumption_support
A_iii$suspectibility_to_bias
table(A_iii$habitat, A_iii$biome)

A_iv<-filter(other, grepl("A_iv)", assumption))
A_iv$assumption_support
A_iv$suspectibility_to_bias
table(A_iv$habitat, A_iv$biome)

medium<-filter(other, grepl("medium", suspectibility_to_bias))
medium$assumption_support
table(medium$habitat, medium$biome)

A_addM<-filter(medium, grepl("A_add", medium$assumption))
A_addM$assumption_support

table(medium$assumption)

###### stats assumption HBii ----
HBii<-filter(coding.all, assumption=="B_ii) AND D_ii) Quantitative availability of plant foods follows rodent_p densities in a delayed density-dependent manner")
HBii<-filter(HBii, include_exclude=="include", na.rm=TRUE) 

HBii<-filter(HBii, grepl("low", suspectibility_to_bias))
HBii_long<-filter(HBii, grepl("temporal_contrast_several_peaks", approach_cycle))
HBii_long
HBii_long$assumption_support
HBii_long$year_end-HBii_long$year_start

HBii<-filter(HBii, grepl("medium", suspectibility_to_bias))
HBii_long<-filter(HBii, grepl("temporal_contrast_several_peaks", approach_cycle))
HBii_long$id
HBii_long$assumption_support
HBii_long$year_end-HBii_long$year_start


###### stats assumption HBi ----
coding.all %>%
  filter(assumption == "B_i) AND D_i) Rodent_p feeding induces quantitative changes in their preferred food plants")

HBi<-filter(coding.all, assumption=="B_i) AND D_i) Rodent_p feeding induces quantitative changes in their preferred food plants")
HBi<-filter(HBi, include_exclude=="include", na.rm=TRUE) 

HBi_obs<-filter(HBi, grepl("observational", study_design))
HBi_obs_fie<-filter(HBi_obs, grepl("field", method))
HBi_obs_fie<-filter(HBi_obs_fie, grepl("low", suspectibility_to_bias))
HBi_obs_fie$assumption_support
HBi_obs_fie$id



###### stats assumption HCi ----

coding.all$assumption<-as.factor(coding.all$assumption)
HCi<-filter(coding.all, assumption=="C_i) Rodent_p feeding induces qualitative changes in their preferred food plants")
HCi<-filter(HCi, include_exclude=="include", na.rm=TRUE) 

table(HCi$food_characteristics)
table(HCi$method, HCi$study_design)

medium<-filter(HCi, grepl("medium", suspectibility_to_bias))
table(medium$food_characteristics)
low<-filter(HCi, grepl("low", suspectibility_to_bias))
table(low$food_characteristics)
table(low$assumption_support)


sum(grepl("silica", HCi$food_characteristics))
sum(grepl("tannin", HCi$food_characteristics))
sum(grepl("catec", HCi$food_characteristics))
sum(grepl("sapon", HCi$food_characteristics))

sum(grepl("silica", low$food_characteristics))
sum(grepl("tannin", low$food_characteristics))
sum(grepl("catec", low$food_characteristics))
sum(grepl("sapon", low$food_characteristics))
sum(grepl("glycos", low$food_characteristics))
sum(grepl("inhib", low$food_characteristics))

sum(grepl("silic", HCi$food_characteristics))
silica<-filter(HCi, grepl("silic", food_characteristics))
low<-filter(silica, grepl("low", suspectibility_to_bias)) # all silica low
low$assumption_support
low$assumption_support
table(low$method, low$study_design)


sum(grepl("phenolics", HCi$food_characteristics))
phenolics<-filter(HCi, grepl("phenolics", food_characteristics))
table(phenolics$method, phenolics$study_design)
table(phenolics$country, phenolics$biome)
phenolics$assumption_support
phenolics$suspectibility_to_bias
low<-filter(phenolics, grepl("low", suspectibility_to_bias))
low$assumption_support
phenolics2<-cbind.data.frame(phenolics$id, phenolics$assumption_support, phenolics$method,phenolics$study_design, phenolics$approach_cycle, phenolics$suspectibility_to_bias)
view(phenolics2)
phenolics2$effect<-c("no_effect", "no_effect", "varies_between_species", "no_effect", "no_effect", "increase", "no_effect", "decrease", "varies_between_species", "no_effect", "varies_between_species")
table(phenolics2$effect, phenolics2$`phenolics$suspectibility_to_bias`)
table(phenolics2$effect, phenolics2$`phenolics$approach_cycle`)


tannins<-filter(HCi, grepl("tanni", food_characteristics))
table(tannins$method, tannins$study_design)
table(tannins$country, tannins$biome)
tannins$assumption_support
tannins$suspectibility_to_bias
view(tannins)

inhib<-filter(HCi, grepl("inhib", food_characteristics))
inhib$assumption_support
table(inhib$method, inhib$study_design)

catechin<-filter(HCi, grepl("catechin", food_characteristics))
catechin$assumption_support

saponin<-filter(HCi, grepl("saponin", food_characteristics))
saponin$assumption_support


protein<-filter(HCi, grepl("protein", food_characteristics))
table(protein$food_characteristics)

# percentage of studies with harmful compounds that also included nutrients
HCi$food_characteristics
length(HCi$food_characteristics)
toto<-c("in_addition", "none", "in_addition", "in_addition", "in_addition",
        "in_addition", "in_addition", "exclude", "none", "in_addition",
        "in_addition", "none", "none", "in_addition", "in_addition",
        "in_addition", "none", "none", "in_addition", "none", "none")

sum(grepl("in_addition", toto))/20
sum(grepl("none", toto))


###### stats assumption HCii ----

HCii<-filter(coding.all, assumption=="C_ii) Plant quality follows rodent_p densities in a delayed density-dependent manner")
HCii<-filter(HCii, include_exclude=="include", na.rm=TRUE) 
HCii$country
HCii$rodent
HCii$biome
HCii$suspectibility_to_bias
table(HCii$method, HCii$study_design)


HCii$food_characteristics

medium<-filter(HCii, grepl("medium", suspectibility_to_bias))
table(medium$food_characteristics)
table(medium$assumption_support)

low<-filter(HCii, grepl("low", suspectibility_to_bias))
table(low$food_characteristics)
table(low$assumption_support)


###### stats assumption HCiv and HDiv----

coding.all$assumption<-as.factor(coding.all$assumption)
HCiv_HDiv<-filter(coding.all, assumption=="C_iv) AND D_iv) At high densities, rodent_i ingest lower quality food than at low densities")
HCiv_HDiv<-filter(HCiv_HDiv, include_exclude=="include", na.rm=TRUE) 

HCiv_HDiv$food_characteristics

cbind(HCiv_HDiv$food_characteristics, HCiv_HDiv$method_comment)

low<-filter(HCiv_HDiv, grepl("low", suspectibility_to_bias))
medium<-filter(HCiv_HDiv, grepl("medium", suspectibility_to_bias))
low$assumption_support
medium$assumption_support

##### stats assumption HCv and HDv ----

HCv_HDv<-filter(coding.all, assumption=="C_v) AND D_v) Lower food quality affects rodent_i health negatively")
HCv_HDv<-filter(HCv_HDv, include_exclude=="include", na.rm=TRUE) 

table(HCv_HDv$rodent)
sum(grepl("Microtus", HCv_HDv$rodent))
sum(grepl("Myodes", HCv_HDv$rodent))
sum(grepl("Lemmus", HCv_HDv$rodent))

table(HCv_HDv$country)
table(HCv_HDv$biome)
table(HCv_HDv$habitat)
table(HCv_HDv$study_design)
table(HCv_HDv$method)
table(HCv_HDv$country, HCv_HDv$biome)
table(HCv_HDv$study_design, HCv_HDv$method)

HCv_HDv$food_characteristics
table(HCv_HDv$impact_on_rodent_individual)

# defences
sum(grepl("silica", HCv_HDv$food_characteristics))
sum(grepl("phenol", HCv_HDv$food_characteristics))
sum(grepl("endoph", HCv_HDv$food_characteristics))
sum(grepl("tanni", HCv_HDv$food_characteristics))

# nutrients
sum(grepl("protein", HCv_HDv$food_characteristics))
sum(grepl("nitro", HCv_HDv$food_characteristics))
sum(grepl("sugar", HCv_HDv$food_characteristics))
sum(grepl("starch", HCv_HDv$food_characteristics))
sum(grepl("calciu", HCv_HDv$food_characteristics))
sum(grepl("phosp", HCv_HDv$food_characteristics))
sum(grepl("sodium", HCv_HDv$food_characteristics))

# other 
other<-filter(HCv_HDv, !(grepl("protein", food_characteristics)))
other<-filter(other, !(grepl("silica", food_characteristics)))i
other<-filter(other, !(grepl("endoph", food_characteristics)))
other<-filter(other, !(grepl("acid", food_characteristics)))
other<-filter(other, !(grepl("sedge", food_characteristics)))
other$food_characteristics
low<-filter(other, grepl("low", suspectibility_to_bias))
low$assumption_support
other$assumption_support

silica<-filter(HCv_HDv, grepl("silica", food_characteristics))
silica2<-filter(HCv_HDv, grepl("sedge", food_characteristics))
silica<-rbind.data.frame(silica, silica2)
low<-filter(silica, grepl("low", suspectibility_to_bias))
low$assumption_support
silica$assumption_support

phenol<-filter(HCv_HDv, grepl("phenol", food_characteristics))
low<-filter(phenol, grepl("low", suspectibility_to_bias))
low$assumption_support
low$food_characteristics
phenol$food_characteristics
phenol$assumption_support

endop<-filter(HCv_HDv, grepl("endop", food_characteristics))
low<-filter(endop, grepl("low", suspectibility_to_bias))
low$assumption_support
low$food_characteristics
endop$food_characteristics
endop$assumption_support

tanni<-filter(HCv_HDv, grepl("tanni", food_characteristics))
low<-filter(tanni, grepl("low", suspectibility_to_bias))
low$assumption_support
low$food_characteristics
tanni$food_characteristics
tanni$assumption_support

## positive compounds
HCv_HDv$food_characteristics

energy<-filter(HCv_HDv, grepl("energy", food_characteristics))
energy2<-filter(HCv_HDv, grepl("caloric", food_characteristics))
energy<-rbind.data.frame(energy, energy2)
low<-filter(energy, grepl("low", suspectibility_to_bias))
low$assumption_support
silica$assumption_support

protein<-filter(HCv_HDv, grepl("protein", food_characteristics))
low<-filter(protein, grepl("low", suspectibility_to_bias))
low$assumption_support
low$food_characteristics
protein$food_characteristics
protein$assumption_support




######## figures ----

#### Figure 4 Distribution of assumptions per hypotheses, count and study design #### 
df.assum <- coding.all %>%
  mutate(assumption = gsub("(.*)).*","\\1", assumption), # removes everything after the last parenthesis to shorten the assumptions
         assumption = gsub("_","", assumption), # shortens further
         assumption = gsub(") AND ","_", assumption), # shortens
         assumption = gsub("(.*)) and Ciii.*","Biii_Ciii_Diii", assumption), #shortens Biii
         assumption = gsub("(.*):.*","\\1", assumption)) %>% # shortens A_additional

  mutate(assumption = strsplit(as.character(assumption), "_")) %>% # some assumptions concern multiple hypotheses
  unnest(assumption) %>% # explode single rows into multiple ones if more than one hypothesis
  
  mutate(general_hypothesis = substr(assumption, 1, 1)) # base hypotheses on type of assumption
  
df.assum <- df.assum %>%
  group_by(study_design, assumption) %>%
  mutate(count = n(),
         assumption = as.factor(assumption))

df.assum <- df.assum %>%
  mutate(assumption = factor(assumption, levels=rev(sort(levels(df.assum$assumption))))) # reorder levels
  
df.assum %>%
  ggplot(aes(assumption, study_design)) +
  geom_point() +
  aes(size = count, color = general_hypothesis) +
  xlab("Assumptions") +
  ylab("Study design") +
  scale_size_continuous(name = "No. of studies",
                        breaks=c(1, 5, 15, 30)) +
  scale_color_manual(name = "Hypothesis", 
                     values = col.hyp,
                     labels = labels) +
  theme_light() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  scale_y_discrete(limits = c("observational", 
                              "experimental", 
                              "quasi-experimental", 
                              "other"),
                   labels = c("Observational",
                              "Experimental",
                              "Quasi-experimental",
                              "Other")) +
  coord_flip()

ggsave("./../../figures/Figure_4_Distribution_of_assumptions.jpg",
       #ggsave("./../../figures/Figure_4_Distribution_of_assumptions.pdf",
       width = 5.5, height = 6)

# Change A_additional to A_other?



# Percent of evidence points that state they aim to test a related hyp.
table(coding.all$question_type == "yes") # n=214 (81%)





#### Figure 5 map ####

df.country <- coding.all %>%
  mutate(country = strsplit(as.character(country), ", ")) %>% # some evidence points have multiple countries
  unnest(country) # explode single rows into multiple ones if more than one country

# map layer
world_map <- map_data("world") # load map data
country_map <- map_data("world", region = unique(df.country$country))

map <- country_map %>%
  ggplot(aes(long, lat)) +
  geom_polygon(data = world_map, aes(x = long, y = lat, group = group),
               color = "grey80", fill = "grey85", size = 0.3) +
  geom_polygon(aes(group = group, fill = region),
               color = "grey80", size = 0.1)+
  scale_fill_viridis_d(guide = "colourbar", aesthetics = "colour")+
  scale_y_continuous(breaks = (-2:2) * 30) +
  scale_x_continuous(breaks = (-4:4) * 45) +
  coord_map("ortho", orientation=c(81, 5, 0)) + # or coord_map(xlim = c(-150,150), ylim = c(5, 75)) 
  theme_void() +
  theme(legend.position = "none",
        plot.margin=unit(c(4,2,3,3),"cm")) +
  scale_fill_grey(start = 0.6, end = 0.6)

# pie charts layer
pie_df <- function(x) {
  df.country %>%
  filter(country == x) %>%
  {prop.table(table(biome = .$biome, country = .$country))} %>%
  as.data.frame()
}

pie_df <- bind_rows( #binds all countries into one dataframe
  lapply(unique(df.country$country), pie_df)) # calculate biome distribution per country 

pie_df <- pie_df %>%
  mutate(biome = factor(biome, levels = c("tundra", "tundra-taiga_ecotone", "taiga", "taiga-temperate_ecotone", "temperate")))

pie_df <- complete(pie_df, country, biome, fill = list(Freq=0)) # add biomes with zero frequency for completeness


pie_plot <- function(x) { # function to plot pie's of all countries
  pie_df2 <- pie_df %>%
    filter(country == x)

ggplot(pie_df2, aes(x="", y=Freq, fill=biome)) +
  ggtitle(paste0(pie_df2$country[1], " (n=", sum(df.country$country == x), ")")) + # PS! This will count a study twice if its been in two countries...
  geom_bar(width = 1, stat = "identity") + 
  coord_polar("y", start=0) +
  theme_void() + 
  theme(legend.position="none", 
        plot.title = element_text(face = "bold", 
                                  hjust = 0.5)) +
  scale_fill_manual(values = c("#006933", "#99FF33", "#FFCC66", "#0066CC", "#00cccc"))
}

pie_plots <-
  lapply(unique(df.country$country), pie_plot) # saves pie plots


# Legend layer
pie_df = df.country %>%
  {table(biome = str_to_title(.$biome))} %>% 
  as.data.frame() %>%
  mutate(biome = str_replace(biome, "_", " "))
pie_df$Label <- paste(pie_df$biome, paste("(", round(((pie_df$Freq/sum(pie_df$Freq))*100),2),"%)", sep = ""), sep=" ")

pie_df <- pie_df %>% 
  mutate(end = 2 * pi * cumsum(Freq)/sum(Freq),
         start = lag(end, default = 0),
         middle = 0.5 * (start + end),
         hjust = ifelse(middle > pi, 1, 0),
         vjust = ifelse(middle < pi/2 | middle > 3 * pi/2, 0, 1))

library(ggforce) # for 'geom_arc_bar'
(legend <- ggplot(pie_df) + 
  geom_arc_bar(aes(x0 = 0, y0 = 0, r0 = 0, r = 1,
                   start = start, end = end, fill = biome)) +
  geom_text(aes(x = 1.05 * sin(middle), y = 1.05 * cos(middle), label = Label,
                hjust = hjust, vjust = vjust)) +
  coord_fixed(clip = 'off') +
  scale_x_continuous(limits = c(-1.5, 1.5),  # Adjust so labels are not cut off
                     name = "", breaks = NULL, labels = NULL) +
  scale_y_continuous(limits = c(-1.1, 1.1),    # Adjust so labels are not cut off
                     name = "", breaks = NULL, labels = NULL) +
  scale_fill_manual(values = c("#006933", "#99FF33", "#FFCC66", "#0066CC", "#00cccc")) +
    ggtitle("Total biome distribution") +
  theme_void() +
  theme(legend.position = "none",
        plot.margin = margin(4, 2, 2, 2, "cm"),
        plot.title = element_text(face = "bold", hjust = 0.5,vjust = 4))
)



# Draw meta-plot
library(cowplot)
cEvPnts = prop.table(log1p(sapply(1:15, function(x) {parse_number(unique(pie_plots[[x]]$labels$title))})))*3

ggdraw(map) + 
  draw_plot(pie_plots[[1]], x = .71, y= .07, width=cEvPnts[1], height=cEvPnts[1]) + #czechia
  draw_plot(pie_plots[[2]], .0, .65, width=cEvPnts[2], height=cEvPnts[2]) + # usa
  draw_plot(pie_plots[[3]], .645, .65, width=cEvPnts[3], height=cEvPnts[3]) + # sweden
  draw_plot(pie_plots[[4]], .632, .21, width=cEvPnts[4], height=cEvPnts[4]) + # germany
  draw_plot(pie_plots[[5]], .465, .8, width=cEvPnts[5], height=cEvPnts[5]) + # japan
  draw_plot(pie_plots[[6]], .636, .31, width=cEvPnts[6], height=cEvPnts[6]) + # norway
  draw_plot(pie_plots[[7]], x= .613, y= .02, width=cEvPnts[7], height=cEvPnts[7]) + # poland
  draw_plot(pie_plots[[8]], .15, .655, width=cEvPnts[8], height=cEvPnts[8]) + # canada
  draw_plot(pie_plots[[9]], .25, .53, width=cEvPnts[9], height=cEvPnts[9]) + # belgium
  draw_plot(pie_plots[[10]], .03, .39, width=cEvPnts[10], height=cEvPnts[10]) + # uk
  draw_plot(pie_plots[[11]], .495, .647, width=cEvPnts[11], height=cEvPnts[11]) + # finland
  draw_plot(pie_plots[[12]], .25, .43, width=cEvPnts[12], height=cEvPnts[12]) + # france
  draw_plot(pie_plots[[13]], .487, .70, width=cEvPnts[13], height=cEvPnts[13]) + # china
  draw_plot(pie_plots[[14]], .40, .72, width=cEvPnts[14], height=cEvPnts[14]) + # greenland
  draw_plot(pie_plots[[15]], .36, .8, width=cEvPnts[15], height=cEvPnts[15]) + # russia
  draw_plot(legend, .0, .0, .53, .53) ; ggsave("./../../figures/Figure_5_map.jpg",
       #ggsave("./../../figures/Figure_4_Distribution_of_assumptions.pdf",
       width = 13, height = 7)






##### statistics for summary_table ----

df <- coding.all %>%
  #filter(assumption == "A_i) Rodent food plants have cyclic variation in flower, seed and berry production OR nutritional quality")
  #filter(assumption == "A_ii) The cyclically available food sources have higher quality than other food items")
  #filter(assumption == "A_iii) At increasing densities, rodent_i ingest a higher quality diet than at low densities")
  #filter(assumption == "A_iv) Increased diet quality leads to increased rodent_i reproduction")
  #filter(assumption == "A_additional: Rodent_p follow plant quality in a delayed density dependent manner")
  #filter(assumption == "B_i) AND D_i) Rodent_p feeding induces quantitative changes in their preferred food plants")
  #filter(assumption == "B_ii) AND D_ii) Quantitative availability of plant foods follows rodent_p densities in a delayed density-dependent manner")
  #filter(assumption == "B_iii) and C_iii) Rodent_i diet composition does not vary with population density AND D_iii) Rodent_i diet composition varies with population density")
  #filter(assumption == "B_v) Lower consumption rate of food affects rodent_i health negatively")
  #filter(assumption == "B_vi) AND C_vi) AND D_vi) Lower rodent_i health affects rodent_p growth rate negatively")
  #filter(assumption == "B_other")  
  #filter(assumption == "C_i) Rodent_p feeding induces qualitative changes in their preferred food plants")
  #filter(assumption == "C_ii) Plant quality follows rodent_p densities in a delayed density-dependent manner")
  #filter(assumption == "C_iv) AND D_iv) At high densities, rodent_i ingest lower quality food than at low densities")
  #filter(assumption == "C_v) AND D_v) Lower food quality affects rodent_i health negatively")
  #filter(assumption == "C_other")
                                                                                                                                  
list(sampleSize = df$study_id %>% length(),
     rodent = df$rodent %>% sub("_[^_]+$", "",.) %>% table(),
     biome = df$biome %>% table(),
     country = df$country %>% table(),
     df %>% group_by(method, study_design) %>% tally(),
     approach = df$approach_cycle %>% table(),
     bias = df$suspectibility_to_bias %>% table()
)









# end
